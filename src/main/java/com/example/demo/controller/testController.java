package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @projectName: demo9
 * @package: com.example.demo.controller
 * @className: testController
 * @author: Lincan
 * @description: TODO
 * @date: 2024/1/10 13:17
 * @version: 1.0
 */

@RestController
@RequestMapping("/hello")
public class testController {


    @GetMapping("/world")
    public String hello_world()
    {
        return "hello world!";
    }
}
